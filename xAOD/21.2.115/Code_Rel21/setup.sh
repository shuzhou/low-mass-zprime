#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS

cd build
rm -rf * 
acmSetup --sourcedir=../source AnalysisBase,21.2.115
## will auto cmake 


### acm sparse_clone_project athena

#acm compile

#cd ..
